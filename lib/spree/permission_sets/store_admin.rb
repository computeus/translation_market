module Spree
  module PermissionSets
    class StoreAdmin < PermissionSets::Base
      def activate!
        can :manage, :all
        can [:manage, :admin], Spree::Order
        can [:manage, :admin], Spree::Product

        cannot :manage, Spree::OptionType
        cannot :manage, Spree::Property
        cannot :manage, Spree::Prototype
        cannot :manage, Spree::Taxonomy
        cannot :manage, Spree::Taxon

        # can [:edit, :admin], :general_settings
        # can [:display, :admin], Spree::TaxCategory
        # can [:display, :admin], Spree::TaxRate
        # can [:display, :admin], Spree::Zone
        # can [:display, :admin], Spree::Country
        # can [:display, :admin], Spree::State
        # can [:display, :admin], Spree::PaymentMethod
        # can [:display, :admin], Spree::Taxonomy
        # can [:display, :admin], Spree::ShippingMethod
        # can [:display, :admin], Spree::ShippingCategory
        # can [:display, :admin], Spree::StockLocation
        # can [:display, :admin], Spree::StockMovement
        # can [:display, :admin], Spree::Tracker
        # can [:display, :admin], Spree::RefundReason
        # can [:display, :admin], Spree::ReimbursementType
        # can [:display, :admin], Spree::ReturnReason
        #
        # can [:admin, :home], :dashboards
        #
        # can :display, Country
        # can :display, OptionType
        # can :display, OptionValue
        # can :create, Spree::Order
        # can :admin, Spree::Order do |order, token|
        #   order.user == user || order.guest_token && token == order.guest_token
        # end

        # can :create, ReturnAuthorization do |return_authorization|
        #   return_authorization.order.user == user
        # end
        # can [:display, :update], CreditCard, user_id: user.id
        # can :display, Product
        # can :display, ProductProperty
        # can :display, Property
        # can :create, Spree.user_class
        # can [:read, :update, :update_email], Spree.user_class, id: user.id
        # can :display, State
        # can :display, StockItem, stock_location: { active: true }
        # can :display, StockLocation, active: true
        # can :display, Taxon
        # can :display, Taxonomy
        # can [:save_in_address_book, :remove_from_address_book], Spree.user_class, id: user.id
        # can [:display, :view_out_of_stock], Variant
        # can :display, Zone
        #
        # can [:display, :admin, :edit, :cart], Spree::Order
        # can [:display, :admin], Spree::Payment
        # can [:display, :admin], Spree::Shipment
        # can [:display, :admin], Spree::Adjustment
        # can [:display, :admin], Spree::LineItem
        # can [:display, :admin], Spree::ReturnAuthorization
        # can [:display, :admin], Spree::CustomerReturn
        # can [:display, :admin], Spree::OrderCancellations
        # can [:display, :admin], Spree::Reimbursement
        # can [:display, :admin], Spree::ReturnItem
        # can [:display, :admin], Spree::Refund
        #
        # can :display, Spree::ReimbursementType
        # can :manage, Spree::Order
        # can :manage, Spree::Payment
        # can :manage, Spree::Shipment
        # can :manage, Spree::Adjustment
        # can :manage, Spree::LineItem
        # can :manage, Spree::ReturnAuthorization
        # can :manage, Spree::CustomerReturn
        # can :manage, Spree::OrderCancellations
        # can :manage, Spree::Reimbursement
        # can :manage, Spree::ReturnItem
        # can :manage, Spree::Refund
        #
        # can [:display, :admin, :edit], Spree::Product
        # can [:display, :admin], Spree::Image
        # can [:display, :admin], Spree::Variant
        # can [:display, :admin], Spree::OptionValue
        # can [:display, :admin], Spree::ProductProperty
        # can [:display, :admin], Spree::OptionType
        # can [:display, :admin], Spree::Property
        # can [:display, :admin], Spree::Prototype
        # can [:display, :admin], Spree::Taxonomy
        # can [:display, :admin], Spree::Taxon
        #
        # can :manage, Spree::Product
        # can :manage, Spree::Image
        # can :manage, Spree::Variant
        # can :manage, Spree::OptionValue
        # can :manage, Spree::ProductProperty
        # can :manage, Spree::OptionType
        # can :manage, Spree::Property
        # can :manage, Spree::Prototype
        # can :manage, Spree::Taxonomy
        # can :manage, Spree::Taxon
        # can :manage, Spree::Classification
        #
        # can [:display, :admin, :edit], Spree::Promotion
        # can [:display, :admin], Spree::PromotionRule
        # can [:display, :admin], Spree::PromotionAction
        # can [:display, :admin], Spree::PromotionCategory
        # can [:display, :admin], Spree::PromotionCode
        #
        # can :manage, Spree::Promotion
        # can :manage, Spree::PromotionRule
        # can :manage, Spree::PromotionAction
        # can :manage, Spree::PromotionCategory
        #
        # can [:display, :admin, :sales_total], :reports
        #
        # can [:display, :admin], Spree::StockItem, stock_location_id: location_ids
        # can :display, Spree::StockLocation, id: location_ids
        #
        # can :manage, Spree::StockItem, stock_location_id: location_ids
        # can :display, Spree::StockLocation, id: location_ids
        #
        # can [:display, :admin], Spree::StockTransfer, source_location_id: location_ids
        # can [:display, :admin], Spree::StockTransfer, destination_location_id: location_ids
        # can :display, Spree::StockLocation, id: location_ids
        #
        # if user.stock_locations.any?
        #   can :display, Spree::StockLocation, id: user_location_ids
        #
        #   can :transfer_from, Spree::StockLocation, id: user_location_ids
        #   can :transfer_to, Spree::StockLocation, id: user_location_ids
        #
        #   can :display, Spree::StockTransfer, source_location_id: user_location_ids
        #   can :manage, Spree::StockTransfer, source_location_id: user_location_ids + [nil], shipped_at: nil
        #   can :manage, Spree::StockTransfer, destination_location_id: user_location_ids
        #   # Do not allow managing transfers to a permitted destination_location_id from an
        #   # unauthorized stock location until it's been shipped to the permitted location.
        #   cannot :manage, Spree::StockTransfer, source_location_id: not_permitted_location_ids, shipped_at: nil
        #
        #   can :display, Spree::TransferItem, stock_transfer: { source_location_id: user_location_ids }
        #   can :manage, Spree::TransferItem, stock_transfer: { source_location_id: user_location_ids + [nil], shipped_at: nil }
        #   can :manage, Spree::TransferItem, stock_transfer: { destination_location_id: user_location_ids }
        #   cannot :manage, Spree::TransferItem, stock_transfer: { source_location_id: not_permitted_location_ids, shipped_at: nil }
        # end
        #
        # can [:display, :admin], Spree::StockItem
        # can :display, Spree::StockLocation
        #
        # can :manage, Spree::StockItem
        # can :display, Spree::StockLocation
        #
        # can [:display, :admin], Spree::StockTransfer
        # can :display, Spree::StockLocation
        #
        # can :manage, Spree::StockTransfer
        # can :manage, Spree::TransferItem
        # can :display, Spree::StockLocation
        #
        # can [:display, :admin, :edit, :addresses, :orders, :items], Spree.user_class
        # can [:display, :admin], Spree::StoreCredit
        # can :display, Spree::Role
        #
        # can [:admin, :display, :create, :update, :save_in_address_book, :remove_from_address_book, :addresses, :orders, :items], Spree.user_class
        #
        # # due to how cancancan filters by associations,
        # # we have to define this twice, once for `accessible_by`
        # can :update_email, Spree.user_class, spree_roles: { id: nil }
        # # and once for `can?`
        # can :update_email, Spree.user_class do |user|
        #   user.spree_roles.none?
        # end
        #
        # cannot [:delete, :destroy], Spree.user_class
        # can :manage, Spree::StoreCredit
        # can :display, Spree::Role
      end
      #
      # private
      #
      # def location_ids
      #   @ids ||= user.stock_locations.pluck(:id)
      # end
      #
      # def user_location_ids
      #   @user_location_ids ||= user.stock_locations.pluck(:id)
      # end
      #
      # def not_permitted_location_ids
      #   @not_permitted_location_ids ||= Spree::StockLocation.where.not(id: user_location_ids).pluck(:id)
      # end
    end
  end
end