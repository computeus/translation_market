namespace :db do
  desc 'Loads our custom sample data after spree samples loaded.'
  task load_translation_market_samples: :environment do
    puts 'Loading Translation Market Samples'

    # Load the sample data files as below
    # Spree::Sample.load_sample('test_sample')

    Spree::Sample.load_sample('languages')
    Spree::Sample.load_sample('language_pairs')
    Spree::Sample.load_sample('order_categories')
    Spree::Sample.load_sample('translation_market_products')
    Spree::Sample.load_sample('translation_market_orders')

    puts 'All Complete'
  end

  desc 'Enhance the main bootstrap task'
  Rake::Task['db:bootstrap'].enhance do
    Rake::Task['db:load_translation_market_samples'].invoke
  end

  task bootstrap: :set_auto_accept do
  end

  task :set_auto_accept do
    ENV['AUTO_ACCEPT'] = 'true'
  end
end