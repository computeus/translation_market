module TranslationMarket
  class Environment
    include Spree::Core::EnvironmentExtension

    attr_accessor :preferences

    def initialize
      @preferences = TranslationMarket::AppConfiguration.new
    end
  end
end