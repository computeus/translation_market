require 'rails_helper'

RSpec.describe TranslationMarket::BackendConfiguration, type: :model do
  it 'returns LANGUAGEPAIR_TABS' do
    expect(TranslationMarket::BackendConfiguration::LANGUAGEPAIR_TABS).to eq([:language_pairs])
  end

  it 'returns ORDERCATEGORY_TABS' do
    expect(TranslationMarket::BackendConfiguration::ORDERCATEGORY_TABS).to eq([:order_categories])
  end

  it 'returns LANGUAGE_TABS' do
    expect(TranslationMarket::BackendConfiguration::LANGUAGE_TABS).to eq([:languages])
  end

  it 'returns default locale' do
    expect(TranslationMarket::BackendConfiguration.new.locale).to eq(Rails.application.config.i18n.default_locale)
  end
end