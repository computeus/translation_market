# == Schema Information
#
# Table name: spree_languages
#
#  id         :integer          not null, primary key
#  code       :string(255)
#  position   :integer
#  active     :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe TranslationMarket::Language, type: :model do
  it { should validate_presence_of(:code) }
  it { should have_many(:source_language_pairs).inverse_of(:source_language).class_name('TranslationMarket::LanguagePair').with_foreign_key(:source_language_id) }
  it { should have_many(:target_language_pairs).inverse_of(:target_language).class_name('TranslationMarket::LanguagePair').with_foreign_key(:target_language_id) }
  it { should have_many(:target_languages).through(:source_language_pairs).conditions(:uniq) }

  it 'should order by position by default' do
    language_1 = FactoryGirl.create(:translation_market_language, code: 'tr')
    language_2 = FactoryGirl.create(:translation_market_language, code: 'en')

    expect(TranslationMarket::Language.all).to eq([language_1, language_2])
  end

  it 'should return language which are source in language_pairs' do
    source_language = FactoryGirl.create(:translation_market_language, code: 'tr')
    target_language = FactoryGirl.create(:translation_market_language, code: 'en')
    TranslationMarket::LanguagePair.create!(source_language: source_language, target_language: target_language)

    expect(TranslationMarket::Language.source_languages.all).to eq([source_language])
  end

  it 'should return language which are target in language_pairs' do
    source_language = FactoryGirl.create(:translation_market_language, code: 'tr')
    target_language = FactoryGirl.create(:translation_market_language, code: 'en')
    TranslationMarket::LanguagePair.create!(source_language: source_language, target_language: target_language)

    expect(TranslationMarket::Language.target_languages.all).to eq([target_language])
  end
end
