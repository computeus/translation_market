require 'rails_helper'

describe TranslationMarket::AppConfiguration, :type => :model do

  let (:prefs) { Rails.application.config.translation_market.preferences }

  it "should be available from the environment" do
    prefs.admin_language_pairs_per_page = 30
    expect(prefs.admin_language_pairs_per_page).to eq(30)
  end

  it 'sets admin_language_pairs_per_page' do
    expect(TranslationMarket::Config[:admin_language_pairs_per_page]).to eq(20)
  end

  it 'sets admin_languages_per_page' do
    expect(TranslationMarket::Config[:admin_languages_per_page]).to eq(20)
  end

  it 'sets admin_order_categories_per_page' do
    expect(TranslationMarket::Config[:admin_order_categories_per_page]).to eq(20)
  end
end

