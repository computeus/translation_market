# == Schema Information
#
# Table name: spree_assets
#
#  id                      :integer          not null, primary key
#  viewable_id             :integer
#  viewable_type           :string(255)
#  attachment_width        :integer
#  attachment_height       :integer
#  attachment_file_size    :integer
#  position                :integer
#  attachment_content_type :string(255)
#  attachment_file_name    :string(255)
#  type                    :string(75)
#  attachment_updated_at   :datetime
#  alt                     :text(65535)
#  created_at              :datetime
#  updated_at              :datetime
#  word_count              :integer          default(0)
#

require 'rails_helper'

RSpec.describe TranslationMarket::Document, type: :model do
  it_behaves_like 'countable'

  describe '#no_attachment_errors' do
    it 'adds more meaningful messages' do
      document = FactoryGirl.build(:translation_market_document)
      allow(document.attachment.errors).to receive(:empty?).and_return(false)
      document.valid?
      expect(document.errors.full_messages.join).to include('Paperclip returned errors for file')
    end
  end
end