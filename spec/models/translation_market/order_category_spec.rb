# == Schema Information
#
# Table name: spree_order_categories
#
#  id                       :integer          not null, primary key
#  name                     :string(255)
#  code                     :string(255)
#  description              :text(65535)
#  position                 :integer
#  parent_id                :integer
#  active                   :boolean          default(FALSE)
#  price_multiplier         :decimal(5, 2)    default(1.0)
#  delivery_time_multiplier :decimal(5, 2)    default(1.0)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

require 'rails_helper'

RSpec.describe TranslationMarket::OrderCategory, type: :model do
  it { should validate_presence_of(:code) }
  it { should validate_length_of(:code).is_at_least(1).is_at_most(20) }
  it { should validate_presence_of(:description) }
  it { should validate_length_of(:description).is_at_least(1).is_at_most(65535) }
  it { should validate_presence_of(:name) }
  it { should validate_length_of(:name).is_at_least(1).is_at_most(255) }

  it { should have_many(:children).inverse_of(:parent).class_name('TranslationMarket::OrderCategory').with_foreign_key(:parent_id) }
  it { should have_one(:option_value).dependent(:destroy).inverse_of(:owner) }
  it { should belong_to(:parent).inverse_of(:children).class_name('TranslationMarket::OrderCategory').with_foreign_key(:parent_id) }

  describe '#create_option_values' do
    it 'increases option value count by 1' do
      order_category = FactoryGirl.create(:translation_market_order_category)
      expect(order_category.option_value.id).to eq(Spree::OptionValue.first.id)
    end
  end
end
