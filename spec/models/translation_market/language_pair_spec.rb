# == Schema Information
#
# Table name: spree_language_pairs
#
#  id                 :integer          not null, primary key
#  source_language_id :integer
#  target_language_id :integer
#  active             :boolean          default(FALSE)
#  word_price         :decimal(10, 2)   default(0.0)
#  translator_price   :decimal(10, 2)   default(0.0)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'rails_helper'

RSpec.describe TranslationMarket::LanguagePair, type: :model do
  it 'should not create a language pair with self' do
    source_language = FactoryGirl.create(:translation_market_language, code: 'tr')
    language_pair = TranslationMarket::LanguagePair.create(source_language: source_language, target_language: source_language)
    expect(language_pair.errors.count).to eq(1)
    expect(language_pair.errors[:target_language_id].first).to eq("can't be the same as source language")
  end

  it 'should create an option value' do
    source_language = FactoryGirl.create(:translation_market_language, code: 'tr')
    target_language = FactoryGirl.create(:translation_market_language, code: 'en')
    language_pair = TranslationMarket::LanguagePair.create!(source_language: source_language, target_language: target_language)
    expect(language_pair.option_value.id).to eq(Spree::OptionValue.first.id)
  end

  describe 'reverse_language_pair' do
    it 'should return the reverse language pair' do
      source_language = FactoryGirl.create(:translation_market_language, code: 'tr')
      target_language = FactoryGirl.create(:translation_market_language, code: 'en')
      language_pair = TranslationMarket::LanguagePair.create!(source_language: source_language, target_language: target_language)
      language_pair_reverse = TranslationMarket::LanguagePair.create!(source_language: target_language, target_language: source_language)

      expect(language_pair.reverse_language_pair).to eq(language_pair_reverse)
      expect(language_pair.reverse).to eq(language_pair_reverse)
    end
  end

  # validate :cant_pair_with_self
end
