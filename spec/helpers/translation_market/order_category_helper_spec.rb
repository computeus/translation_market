require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the Spree::HomeHelper. For example:
#
# describe Spree::HomeHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe TranslationMarket::OrderCategoryHelper, type: :helper do
  describe '#select_for_order_categories' do
    it 'returns order categories selectbox' do
      @order_categories = []
      expect(select_for_order_categories).to be =~ /<select name=\"order_category\" id=\"order_category\" class=\"form-control\">/
    end
  end
end
