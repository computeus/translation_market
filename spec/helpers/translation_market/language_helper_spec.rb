require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the Spree::HomeHelper. For example:
#
# describe Spree::HomeHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe TranslationMarket::LanguageHelper, type: :helper do
  describe '#select_for_source_languages' do
    it 'returns source languages selectbox' do
      @source_languages = []
      expect(select_for_source_languages).to be =~ /<select name=\"source_language\" id=\"source_language\" class=\"form-control\">/
    end
  end

  describe '#select_for_target_languages' do
    it 'returns target languages selectbox' do
      @target_languages = []
      expect(select_for_target_languages).to eq('<select name="target_languages[]" id="target_languages" class="form-control" multiple="multiple"></select>')
    end
  end
end
