require 'rails_helper'

RSpec.describe Spree::HomeController, type: :controller do
  before :each do
    user = mock_model(Spree.user_class, last_incomplete_spree_order: nil, spree_api_key: 'fake')
    allow(controller).to receive_messages try_spree_current_user: user
  end

  describe "GET #index" do
    it "returns http success" do
      spree_get :index
      expect(response).to have_http_status(:success)
    end
  end

  context "layout" do
    it "renders default layout" do
      spree_get :index
      expect(response).to render_template(layout: 'layouts/application')
    end
  end
end
