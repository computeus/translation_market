RSpec.shared_context "shared_view_examples", type: :view do
  before do
    @ability = Object.new
    @ability.extend(CanCan::Ability)
    allow(controller).to receive(:current_ability).and_return(@ability)

    sign_in FactoryGirl.create(:spree_admin_user)

    params[:q] ||= {}
  end
end