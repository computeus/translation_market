require 'rails_helper'
require 'shared/shared_admin_view_examples'

RSpec.describe "translation_market/admin/order_categories/index", type: :view do
  it "renders a list of order_categories" do
    FactoryGirl.create(:translation_market_order_category, name: 'Category1')
    FactoryGirl.create(:translation_market_order_category, name: 'Category2')

    @search = assign(:search, TranslationMarket::OrderCategory.all.ransack)

    @order_categories = assign(:order_categories, @search.result.page(params[:page]).per(TranslationMarket::Config[:admin_order_categories_per_page]))

    assign(:order_categories_for_search,
           @order_categories.collect { |order_category| [order_category.name, order_category.id] }
    )

    render

    expect(rendered).to match(/Category1/)
    expect(rendered).to match(/Category2/)
  end
end
