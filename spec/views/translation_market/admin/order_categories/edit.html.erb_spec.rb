require 'rails_helper'
require 'shared/shared_admin_view_examples'

RSpec.describe "translation_market/admin/order_categories/edit", type: :view do
  it "renders the edit translation_market_order_category form" do
    @order_category = assign(:order_category, FactoryGirl.create(:translation_market_order_category))

    render

    assert_select "form[action=?][method=?]", admin_order_category_path(@order_category), "post" do
      assert_select "input#translation_market_order_category_name[name=?]", "translation_market_order_category[name]"
    end
  end
end
