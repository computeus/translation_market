require 'rails_helper'
require 'shared/shared_admin_view_examples'

RSpec.describe "translation_market/admin/languages/edit", type: :view do
  it "renders the edit translation_market_language form" do
    @language = assign(:language, FactoryGirl.create(:translation_market_language))

    render

    assert_select "form[action=?][method=?]", admin_language_path(@language), "post" do
      assert_select "input#translation_market_language_active[name=?]", "translation_market_language[active]"
    end
  end
end