require 'rails_helper'
require 'shared/shared_admin_view_examples'

RSpec.describe "translation_market/admin/languages/index", type: :view do
  it "renders a list of translation_market/admins" do
    language_1 = FactoryGirl.create(:translation_market_language, code: :tr)
    language_2 = FactoryGirl.create(:translation_market_language, code: :en)

    @search = assign(:search, TranslationMarket::Language.all.ransack)

    @languages = assign(:languages, @search.result.page(params[:page]).per(TranslationMarket::Config[:admin_languages_per_page]))

    assign(:languages_for_search,
           @languages.collect { |language| [I18n.t("languages.#{language.code}"), language.id] }
    )

    render

    expect(rendered).to match(/#{I18n.t("languages.#{language_1.code}")}/)
    expect(rendered).to match(/#{I18n.t("languages.#{language_2.code}")}/)
  end
end
