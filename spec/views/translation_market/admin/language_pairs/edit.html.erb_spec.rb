require 'rails_helper'
require 'shared/shared_admin_view_examples'

RSpec.describe "translation_market/admin/language_pairs/edit", type: :view do
  it "renders the edit translation_market_admin form" do
    @language_pair = assign(:language_pair, FactoryGirl.create(:translation_market_language_pair))
    @languages = assign(:languages, TranslationMarket::Language.active.asc.collect { |language| [I18n.t("languages.#{language.code}"), language.id] } )

    render

    assert_select "form[action=?][method=?]", admin_language_pair_path(@language_pair), "post" do
      assert_select "input#translation_market_language_pair_active[name=?]", "translation_market_language_pair[active]"
    end
  end
end
