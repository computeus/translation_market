require 'rails_helper'
require 'shared/shared_admin_view_examples'

RSpec.describe "translation_market/admin/language_pairs/index", type: :view do
  it "renders a list of translation_market/admins" do
    language_pair_1 = FactoryGirl.create(:translation_market_language_pair)
    language_pair_2 = FactoryGirl.create(:translation_market_language_pair)

    @search = assign(:search, TranslationMarket::LanguagePair.all.ransack)

    @language_pairs = assign(:language_pairs, @search.result.page(params[:page]).per(TranslationMarket::Config[:admin_language_pairs_per_page]))

    assign(:languages,
           TranslationMarket::Language.active.asc.collect { |language| [I18n.t("languages.#{language.code}"), language.id] }
    )

    render

    expect(rendered).to match(/#{number_to_currency(language_pair_1.word_price)}/)
    expect(rendered).to match(/#{number_to_currency(language_pair_2.word_price)}/)
  end
end
