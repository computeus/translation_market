require 'rails_helper'

RSpec.describe "translation_market/stores/new", type: :view do
  it "renders new spree_store form" do
    assign(:spree_user, FactoryGirl.build(:spree_store_admin_user, store: FactoryGirl.build(:spree_store)))

    render

    assert_select "form[action=?][method=?]", stores_path, "post" do
      assert_select "input#user_store_attributes_name[name=?]", "user[store_attributes][name]"
    end
  end
end