require 'rails_helper'

RSpec.describe "translation_market/stores/edit", type: :view do
  it "renders the edit spree_store form" do
    @spree_user = assign(:spree_user, FactoryGirl.create(:spree_store_admin_user))
    @spree_store = assign(:spree_store, FactoryGirl.create(:spree_store))

    render

    assert_select "form[action=?][method=?]", store_path(@spree_store), "post" do
      assert_select "input#store_name[name=?]", "store[name]"
    end
  end
end