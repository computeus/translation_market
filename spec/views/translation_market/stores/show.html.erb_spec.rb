require 'rails_helper'

RSpec.describe "translation_market/stores/show", type: :view do
  it "renders attributes in <p>" do
    @spree_store = assign(:spree_store, FactoryGirl.create(:spree_store))

    render
    expect(rendered).to match(/Name/)
  end
end