require 'rails_helper'

RSpec.describe Spree::OptionType, type: :model do
  describe '.find_or_create_for_language_pair' do
    context 'when there is an option type for language pair' do
      it 'returns option type for language pair' do
        option_type = FactoryGirl.create(:option_type, name: 'language-pair', presentation: 'Language Pair')
        expect(Spree::OptionType.find_or_create_for_language_pair).to eq(option_type)
      end
    end

    context 'when there is no option type for language pair' do
      it 'creates and returns option type for language pair' do
        expect { Spree::OptionType.find_or_create_for_language_pair }.to change { Spree::OptionType.count }.by(1)
      end
    end
  end
end