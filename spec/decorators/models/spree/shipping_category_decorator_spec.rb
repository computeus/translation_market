require 'rails_helper'

RSpec.describe Spree::ShippingCategory, type: :model do
  describe '.default' do
    it 'returns default shipping category' do
      shipping_category = FactoryGirl.create(:shipping_category, name: 'Default')
      expect(Spree::ShippingCategory.default).to eq(shipping_category)
    end
  end
end