require 'rails_helper'

RSpec.describe Spree::OptionValue, type: :model do
  it { should belong_to(:owner).inverse_of(:option_value) }

  describe '.from_language_pairs' do
    it 'returns option values of given language pairs' do
      language_pair1 = FactoryGirl.create(:translation_market_language_pair)
      language_pair2 = FactoryGirl.create(:translation_market_language_pair)

      expect(Spree::OptionValue.from_language_pairs(TranslationMarket::LanguagePair.all)).to eq([language_pair1.option_value.id, language_pair2.option_value.id])
    end
  end
end