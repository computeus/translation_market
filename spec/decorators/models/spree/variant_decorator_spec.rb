require 'rails_helper'

RSpec.describe Spree::Variant, type: :model do
  it { should have_many(:documents).dependent(:destroy).class_name('TranslationMarket::Document').order(:position) }

  describe '#set_variant_price' do
    it 'sets the price' do
      language_pair  = FactoryGirl.create(:translation_market_language_pair, word_price: 2)
      order_category = FactoryGirl.create(:translation_market_order_category, price_multiplier: 3)
      FactoryGirl.create(:shipping_category, name: 'Default')
      product = FactoryGirl.create(:spree_base_product, option_values_hash: { Spree::OptionType.find_or_create_for_language_pair.id => [language_pair.option_value.id], Spree::OptionType.find_or_create_for_order_category.id => [order_category.option_value.id] }, documents_attributes: [{ attachment: File.open(Rails.root.join('spec', 'factories', 'files', 'document_file.txt')) }])

      expect(product.variants.first.price).to eq(12)
    end
  end
end