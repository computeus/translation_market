require "rails_helper"

RSpec.describe TranslationMarket::Admin::OrderCategoriesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "admin/order_categories").to route_to("translation_market/admin/order_categories#index")
    end

    it "routes to #edit" do
      expect(get: "admin/order_categories/1/edit").to route_to("translation_market/admin/order_categories#edit", id: "1")
    end

    it "routes to #update via PUT" do
      expect(put: "admin/order_categories/1").to route_to("translation_market/admin/order_categories#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "admin/order_categories/1").to route_to("translation_market/admin/order_categories#update", id: "1")
    end
  end
end
