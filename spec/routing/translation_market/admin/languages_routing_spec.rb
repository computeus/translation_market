require "rails_helper"

RSpec.describe TranslationMarket::Admin::LanguagesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "admin/languages").to route_to("translation_market/admin/languages#index")
    end

    it "routes to #edit" do
      expect(get: "admin/languages/1/edit").to route_to("translation_market/admin/languages#edit", id: "1")
    end

    it "routes to #update via PUT" do
      expect(put: "admin/languages/1").to route_to("translation_market/admin/languages#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "admin/languages/1").to route_to("translation_market/admin/languages#update", id: "1")
    end
  end
end
