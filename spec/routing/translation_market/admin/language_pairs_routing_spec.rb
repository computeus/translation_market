require "rails_helper"

RSpec.describe TranslationMarket::Admin::LanguagePairsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "admin/language_pairs").to route_to("translation_market/admin/language_pairs#index")
    end

    it "routes to #new" do
      expect(get: "admin/language_pairs/new").to route_to("translation_market/admin/language_pairs#new")
    end

    it "routes to #edit" do
      expect(get: "admin/language_pairs/1/edit").to route_to("translation_market/admin/language_pairs#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "admin/language_pairs").to route_to("translation_market/admin/language_pairs#create")
    end

    it "routes to #update via PUT" do
      expect(put: "admin/language_pairs/1").to route_to("translation_market/admin/language_pairs#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "admin/language_pairs/1").to route_to("translation_market/admin/language_pairs#update", id: "1")
    end
  end
end
