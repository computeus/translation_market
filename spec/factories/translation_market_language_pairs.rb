# == Schema Information
#
# Table name: spree_language_pairs
#
#  id                 :integer          not null, primary key
#  source_language_id :integer
#  target_language_id :integer
#  active             :boolean          default(FALSE)
#  word_price         :decimal(10, 2)   default(0.0)
#  translator_price   :decimal(10, 2)   default(0.0)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :translation_market_language_pair, class: 'TranslationMarket::LanguagePair' do
    association :source_language, factory: :translation_market_language
    association :target_language, factory: :translation_market_language
    active false
  end
end
