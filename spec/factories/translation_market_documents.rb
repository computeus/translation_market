FactoryGirl.define do
  factory :translation_market_document, class: 'TranslationMarket::Document' do
    attachment File.open(Rails.root.join('spec', 'factories', 'files', 'document_file.txt'))
    association :viewable, factory: :spree_base_product
  end
end
