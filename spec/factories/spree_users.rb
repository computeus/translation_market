FactoryGirl.define do
  factory :spree_user, class: Spree.user_class do
    email { generate(:random_email) }
    login { email }
    password 'secret'
    password_confirmation { password }
    authentication_token { generate(:user_authentication_token) } if Spree.user_class.attribute_method? :authentication_token

    trait :with_api_key do
      after(:create) do |user, _|
        user.generate_spree_api_key!
      end
    end

    factory :spree_admin_user do
      spree_roles { [Spree::Role.find_by(name: 'admin') || create(:role, name: 'admin')] }
    end

    factory :spree_store_admin_user do
      spree_roles { [Spree::Role.find_by(name: 'store_admin') || create(:role, name: 'store_admin')] }
    end

    factory :spree_user_with_addresses do |u|
      bill_address
      ship_address
    end
  end
end
