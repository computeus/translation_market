# == Schema Information
#
# Table name: spree_languages
#
#  id         :integer          not null, primary key
#  code       :string(255)
#  position   :integer
#  active     :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :translation_market_language, class: 'TranslationMarket::Language' do
    sequence(:code) { |n| "code#{n}" }
    active false
  end
end
