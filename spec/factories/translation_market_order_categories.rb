# == Schema Information
#
# Table name: spree_order_categories
#
#  id                       :integer          not null, primary key
#  name                     :string(255)
#  code                     :string(255)
#  description              :text(65535)
#  position                 :integer
#  parent_id                :integer
#  active                   :boolean          default(FALSE)
#  price_multiplier         :decimal(5, 2)    default(1.0)
#  delivery_time_multiplier :decimal(5, 2)    default(1.0)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

FactoryGirl.define do
  factory :translation_market_order_category, class: 'TranslationMarket::OrderCategory' do
    name 'Genel'
    code 'general'
    description 'Genel kategorisi'
  end
end
