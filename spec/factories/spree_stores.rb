FactoryGirl.define do
  factory :spree_store, class: Spree::Store do
    sequence(:code) { |i| "spree_#{i}" }
    sequence(:name) { |i| "Spree Test Store #{i}" }
    sequence(:url) { |i| "www.example#{i}.com" }
    mail_from_address 'spree@example.org'
    association :admin, factory: :spree_store_admin_user
  end
end