Spree::Sample.load_sample("tax_categories")
Spree::Sample.load_sample("shipping_categories")

{
  "UPS Ground (USD)"  => [5, "TRY"],
  "UPS Ground (EU)"   => [5, "TRY"],
  "UPS One Day (USD)" => [15, "TRY"],
  "UPS Two Day (USD)" => [10, "TRY"],
  "UPS Ground (EUR)"  => [8, "EUR"]
}.each do |shipping_method_name, (price, currency)|
  shipping_method                        = Spree::ShippingMethod.find_by_name!(shipping_method_name)
  shipping_method.calculator.preferences = {
    amount:   price,
    currency: currency
  }
  shipping_method.calculator.save!
  shipping_method.save!
end

tax_category      = Spree::TaxCategory.find_by_name!('Default')
shipping_category = Spree::ShippingCategory.find_by_name!('Default')

default_attrs = {
  available_on: Time.current
}

example_files = Dir.glob(Rails.root.join('db', 'samples', 'files', '*'))

language_pair_store_3_ids = TranslationMarket::LanguagePair.active.where(store_id: 3).pluck(:id)
language_pair_store_4_ids = TranslationMarket::LanguagePair.active.where(store_id: 4).pluck(:id)

products = [
  {
    option_values_hash:   {
      Spree::OptionType.find_or_create_for_language_pair.id.to_s  => Spree::OptionValue.from_language_pairs(TranslationMarket::LanguagePair.where(id: language_pair_store_3_ids.sample(2))),
      Spree::OptionType.find_or_create_for_order_category.id.to_s => [TranslationMarket::OrderCategory.all.sample.option_value.id]
    },
    documents_attributes: [{ attachment: File.open(example_files.sample) }],
    tax_category:         tax_category,
    shipping_category:    shipping_category,
    store_id:             3
  },
  {
    option_values_hash:   {
      Spree::OptionType.find_or_create_for_language_pair.id.to_s  => Spree::OptionValue.from_language_pairs(TranslationMarket::LanguagePair.where(id: language_pair_store_3_ids.sample(2))),
      Spree::OptionType.find_or_create_for_order_category.id.to_s => [TranslationMarket::OrderCategory.all.sample.option_value.id]
    },
    documents_attributes: [{ attachment: File.open(example_files.sample) }],
    tax_category:         tax_category,
    shipping_category:    shipping_category,
    store_id:             3
  },
  {
    option_values_hash:   {
      Spree::OptionType.find_or_create_for_language_pair.id.to_s  => Spree::OptionValue.from_language_pairs(TranslationMarket::LanguagePair.where(id: language_pair_store_4_ids.sample(2))),
      Spree::OptionType.find_or_create_for_order_category.id.to_s => [TranslationMarket::OrderCategory.all.sample.option_value.id]
    },
    documents_attributes: [{ attachment: File.open(example_files.sample) }],
    tax_category:         tax_category,
    shipping_category:    shipping_category,
    store_id:             4
  },
  {
    option_values_hash:   {
      Spree::OptionType.find_or_create_for_language_pair.id.to_s  => Spree::OptionValue.from_language_pairs(TranslationMarket::LanguagePair.where(id: language_pair_store_4_ids.sample(2))),
      Spree::OptionType.find_or_create_for_order_category.id.to_s => [TranslationMarket::OrderCategory.all.sample.option_value.id]
    },
    documents_attributes: [{ attachment: File.open(example_files.sample) }],
    tax_category:         tax_category,
    shipping_category:    shipping_category,
    store_id:             4
  },
  {
    option_values_hash:   {
      Spree::OptionType.find_or_create_for_language_pair.id.to_s  => Spree::OptionValue.from_language_pairs(TranslationMarket::LanguagePair.where(id: language_pair_store_4_ids.sample(2))),
      Spree::OptionType.find_or_create_for_order_category.id.to_s => [TranslationMarket::OrderCategory.all.sample.option_value.id]
    },
    documents_attributes: [{ attachment: File.open(example_files.sample) }],
    tax_category:         tax_category,
    shipping_category:    shipping_category,
    store_id:             4
  }
]

Spree::Config[:currency] = 'TRY'

products.each do |product_attrs|
  Spree::Product.create!(default_attrs.merge(product_attrs))
end