languages = TranslationMarket::Language.first(10)

languages.each do |l|
  languages.each do |le|
    TranslationMarket::LanguagePair.create(source_language: l, target_language: le, word_price: 1, translator_price: 0.25, store_id: 3)
    TranslationMarket::LanguagePair.create(source_language: l, target_language: le, word_price: 1, translator_price: 0.25, store_id: 4)
  end
end

TranslationMarket::LanguagePair.find_each do |language_pair|
  language_pair.active = true
  language_pair.save
end