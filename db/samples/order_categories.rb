TranslationMarket::OrderCategory.create!(
  name:                     'Genel',
  code:                     'general',
  description:              'Genel',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Pazarlama',
  code:                     'marketing',
  description:              'Pazarlama',
  price_multiplier:         1.25,
  delivery_time_multiplier: 1.25,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Otomotiv',
  code:                     'automotive',
  description:              'Otomotiv',
  price_multiplier:         1.25,
  delivery_time_multiplier: 1.25,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Yazılım / IT',
  code:                     'software',
  description:              'Yazılım / IT',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Akademik',
  code:                     'academical',
  description:              'Akademik',
  price_multiplier:         1.25,
  delivery_time_multiplier: 1.25,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Elektronik',
  code:                     'electronic',
  description:              'Elektronik',
  price_multiplier:         1.25,
  delivery_time_multiplier: 1.25,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Elektrik',
  code:                     'elektric',
  description:              'Elektrik',
  price_multiplier:         1.25,
  delivery_time_multiplier: 1.25,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Web Sitesi',
  code:                     'website',
  description:              'Web Sitesi',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Medikal',
  code:                     'medical',
  description:              'Medikal',
  price_multiplier:         2,
  delivery_time_multiplier: 1.5,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Hukuk',
  code:                     'law',
  description:              'Hukuk',
  price_multiplier:         3,
  delivery_time_multiplier: 1.5,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Kimya',
  code:                     'chemistry',
  description:              'Kimya',
  price_multiplier:         1.25,
  delivery_time_multiplier: 1.25,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'İnşaat Mühendisliği',
  code:                     'civil_engineering',
  description:              'İnşaat Mühendisliği',
  price_multiplier:         1.25,
  delivery_time_multiplier: 1.25,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Mekanik / İmalat',
  code:                     'mechanic',
  description:              'Mekanik / İmalat',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Medya / Yayıncılık',
  code:                     'media',
  description:              'Medya / Yayıncılık',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Mimarlık',
  code:                     'architecture',
  description:              'Mimarlık',
  price_multiplier:         1.25,
  delivery_time_multiplier: 1.25,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Muhasebe ve Finans',
  code:                     'finance',
  description:              'Muhasebe ve Finans',
  price_multiplier:         1.3,
  delivery_time_multiplier: 1.3,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Kişisel Yazışma / Mektup',
  code:                     'letter',
  description:              'Kişisel Yazışma / Mektup',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Sertifika / Diploma / Özgeçmiş',
  code:                     'certificate',
  description:              'Sertifika / Diploma / Özgeçmiş',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'E-ticaret',
  code:                     'ecommerce',
  description:              'E-ticaret',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Oyun',
  code:                     'game',
  description:              'Oyun',
  price_multiplier:         2,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Mobil Uygulama',
  code:                     'mobile',
  description:              'Mobil Uygulama',
  price_multiplier:         2,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Seyahat ve Turizm',
  code:                     'traveling',
  description:              'Seyahat ve Turizm',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)

TranslationMarket::OrderCategory.create!(
  name:                     'Edebi Metin',
  code:                     'literature',
  description:              'Edebi Metin',
  price_multiplier:         1,
  delivery_time_multiplier: 1,
  active:                   1
)
