Spree::Sample.load_sample("addresses")

payment_method = Spree::PaymentMethod::Check.first!

orders = []
orders << Spree::Order.create!(
  shipping_address: Spree::Address.first,
  billing_address:  Spree::Address.last,
  user:             Spree::User.customer.first,
  created_by:       Spree::User.customer.first,
  store_id:         3
)

orders << Spree::Order.create!(
  shipping_address: Spree::Address.first,
  billing_address:  Spree::Address.last,
  user:             Spree::User.customer.first,
  created_by:       Spree::User.customer.first,
  store_id:         4
)

orders[0].store.products.all[0].variants.each do |variant|
  orders[0].contents.add(variant, 1)
end

orders[0].store.products.all[1].variants.each do |variant|
  orders[0].contents.add(variant, 1)
end

orders[1].store.products.all[0].variants.each do |variant|
  orders[1].contents.add(variant, 1)
end

orders[1].store.products.all[1].variants.each do |variant|
  orders[1].contents.add(variant, 1)
end

orders[1].store.products.all[2].variants.each do |variant|
  orders[1].contents.add(variant, 1)
end

orders.each do |order|
  order.payments.create!(payment_method: payment_method)

  order.next! while !order.can_complete?
  order.complete!
end