if Spree::Store.find_by(code: :spree).present?
  store       = Spree::Store.find_by(code: :spree)
  store.admin = Spree::User.admin.first
  store.save!
end

unless Spree::Store.where(code: :spree).exists?
  Spree::Store.new do |s|
    s.code              = 'spree'
    s.name              = 'Spree Demo Site'
    s.url               = 'demo.spreecommerce.com'
    s.mail_from_address = 'spree@example.com'
    s.admin             = Spree::User.admin.first
  end.save!
end

Spree::Store.create!(
  name:              "Translation Market",
  code:              "translation-market",
  url:               "tm.dev.com",
  mail_from_address: "tm@example.com",
  admin:             Spree::User.store_admin.first
)

Spree::Store.create!(
  name:              "Çeviri Market",
  code:              "ceviri-market",
  url:               "ceviri.dev.com",
  mail_from_address: "ceviri@example.com",
  admin:             Spree::User.store_admin.last
)