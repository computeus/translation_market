class CreateTranslationMarketLanguages < ActiveRecord::Migration
  def change
    create_table :translation_market_languages do |t|
      t.string :code
      t.integer :position
      t.boolean :active, default: false

      t.timestamps null: false
    end
    add_index :translation_market_languages, :code
    add_index :translation_market_languages, :position
    add_index :translation_market_languages, :active
  end
end
