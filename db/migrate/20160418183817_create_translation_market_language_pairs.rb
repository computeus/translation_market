class CreateTranslationMarketLanguagePairs < ActiveRecord::Migration
  def change
    create_table :translation_market_language_pairs do |t|
      t.integer :source_language_id
      t.integer :target_language_id
      t.boolean :active, default: false
      t.decimal :word_price, precision: 10, scale: 2, default: 0
      t.decimal :translator_price, precision: 10, scale: 2, default: 0

      t.timestamps null: false
    end
    add_index :translation_market_language_pairs, :source_language_id
    add_index :translation_market_language_pairs, :target_language_id
  end
end
