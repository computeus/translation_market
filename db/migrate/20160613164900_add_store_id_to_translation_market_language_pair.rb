class AddStoreIdToTranslationMarketLanguagePair < ActiveRecord::Migration
  def change
    add_column :translation_market_language_pairs, :store_id, :integer
    add_index :translation_market_language_pairs, :store_id
  end
end
