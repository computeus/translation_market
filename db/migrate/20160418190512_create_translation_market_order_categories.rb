class CreateTranslationMarketOrderCategories < ActiveRecord::Migration
  def change
    create_table :translation_market_order_categories do |t|
      t.string :name
      t.string :code
      t.text :description
      t.integer :position
      t.integer :parent_id, default: nil
      t.boolean :active, default: false
      t.decimal :price_multiplier, default: 1, precision: 5, scale: 2
      t.decimal :delivery_time_multiplier, default: 1, precision: 5, scale: 2

      t.timestamps null: false
    end
    add_index :translation_market_order_categories, :code
    add_index :translation_market_order_categories, :position
    add_index :translation_market_order_categories, :parent_id
    add_index :translation_market_order_categories, :active
  end
end
