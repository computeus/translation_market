class AddOwnerToOptionValue < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :owner_id, :integer
    add_index :spree_option_values, :owner_id
    add_column :spree_option_values, :owner_type, :string
    add_index :spree_option_values, :owner_type
  end
end
