class AddWordCountToSpreeDocument < ActiveRecord::Migration
  def change
    add_column :spree_assets, :word_count, :integer, default: 0
  end
end
