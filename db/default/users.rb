require 'highline/import'

# see last line where we create an admin if there is none, asking for email and password
def prompt_for_admin_password
  if ENV['ADMIN_PASSWORD']
    password = ENV['ADMIN_PASSWORD'].dup
    say "Admin Password #{password}"
  else
    password = ask('Password [123123123]: ') do |q|
      q.echo = false
      q.validate = /^(|.{5,40})$/
      q.responses[:not_valid] = 'Invalid password. Must be at least 5 characters long.'
      q.whitespace = :strip
    end
    password = '123123123' if password.blank?
  end

  password
end

def prompt_for_admin_email
  if ENV['ADMIN_EMAIL']
    email = ENV['ADMIN_EMAIL'].dup
    say "Admin User #{email}"
  else
    email = ask('Email [admin@tm.dev]: ') do |q|
      q.echo = true
      q.whitespace = :strip
    end
    email = 'admin@tm.dev' if email.blank?
  end

  email
end

def create_admin_user
  if ENV['AUTO_ACCEPT']
    password = '123123123'
    email = 'admin@tm.dev'
  else
    puts 'Create the admin user (press enter for defaults).'
    email = prompt_for_admin_email
    password = prompt_for_admin_password
  end
  attributes = {
    :password => password,
    :password_confirmation => password,
    :email => email,
    :login => email
  }

  load 'spree/user.rb'

  if Spree::User.find_by_email(email)
    say "\nWARNING: There is already a user with the email: #{email}, so no account changes were made.  If you wish to create an additional admin user, please run rake spree_auth:admin:create again with a different email.\n\n"
  else
    admin = Spree::User.new(attributes)
    if admin.save
      role = Spree::Role.find_or_create_by(name: 'admin')
      admin.spree_roles << role
      admin.save
      admin.generate_spree_api_key!
      say "Done!"
    else
      say "There was some problems with persisting new admin user:"
      admin.errors.full_messages.each do |error|
        say error
      end
    end
  end
end

def create_customer_user
  password = '123123123'
  email = 'customer@tm.dev'

  attributes = {
    :password => password,
    :password_confirmation => password,
    :email => email,
    :login => email
  }

  # load 'spree/user.rb'

  if Spree::User.find_by_email(email)
    say "\nWARNING: There is already a user with the email: #{email}, so no account changes were made.\n\n"
  else
    user = Spree::User.new(attributes)
    if user.save
      role = Spree::Role.find_or_create_by(name: 'user')
      user.spree_roles << role
      user.save
      user.generate_spree_api_key!
      say "Done!"
    else
      say "There was some problems with persisting new customer user:"
      user.errors.full_messages.each do |error|
        say error
      end
    end
  end
end

def create_store_admin(email = 'store_admin@tm.dev')
  password = '123123123'

  attributes = {
    :password => password,
    :password_confirmation => password,
    :email => email,
    :login => email
  }

  if Spree::User.find_by_email(email)
    say "\nWARNING: There is already a user with the email: #{email}, so no account changes were made.\n\n"
  else
    store_admin = Spree::User.new(attributes)
    if store_admin.save
      role = Spree::Role.find_or_create_by(name: 'store_admin')
      store_admin.spree_roles << role
      store_admin.save
      store_admin.generate_spree_api_key!
      say "Done!"
    else
      say "There was some problems with persisting new customer user:"
      store_admin.errors.full_messages.each do |error|
        say error
      end
    end
  end
end

if Spree::User.admin.empty?
  create_admin_user
else
  puts 'Admin user has already been previously created.'
  if agree('Would you like to create a new admin user? (yes/no)')
    create_admin_user
  else
    puts 'No admin user created.'
  end
end

create_customer_user
create_store_admin
create_store_admin('store_admin@ceviri.dev')
