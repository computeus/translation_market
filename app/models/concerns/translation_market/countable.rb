# Counts the word count of uploaded file
module TranslationMarket
  module Countable
    extend ActiveSupport::Concern

    included do
      before_save :count_words
    end

    # Counts words for document
    def count_words
      if viewable_type == 'Spree::Product'
        temporary       = attachment.queued_for_write[:original]
        filename        = temporary.path unless temporary.nil?
        filename        = attachment.path if filename.blank?
        self.word_count = get_file_word_count(filename) if word_count == 0
      end
    end

    # Open txt file and read contents
    # Then split with *space* and return the length
    def get_file_word_count(filename)
      File.read(filename).split.length
    end
  end
end