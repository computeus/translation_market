# == Schema Information
#
# Table name: spree_order_categories
#
#  id                       :integer          not null, primary key
#  name                     :string(255)
#  code                     :string(255)
#  description              :text(65535)
#  position                 :integer
#  parent_id                :integer
#  active                   :boolean          default(FALSE)
#  price_multiplier         :decimal(5, 2)    default(1.0)
#  delivery_time_multiplier :decimal(5, 2)    default(1.0)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

module TranslationMarket
  class OrderCategory < TranslationMarket::Base
    acts_as_list

    has_many :children, class_name: 'TranslationMarket::OrderCategory', foreign_key: :parent_id, inverse_of: :parent
    has_one :option_value, as: :owner, dependent: :destroy, inverse_of: :owner, class_name: 'Spree::OptionValue'

    belongs_to :parent, class_name: 'TranslationMarket::OrderCategory', foreign_key: :parent_id, inverse_of: :children

    validates :name, length: { in: 1..255 }, presence: true
    validates :description, length: { in: 1..65535 }, presence: true
    validates :parent, presence: true, allow_nil: true
    validates :code, length: { in: 1..20 }, presence: true
    validates :active, inclusion: { in: [true, false] }

    self.whitelisted_ransackable_attributes = %w[name description parent_id position price_multiplier delivery_time_multiplier active activated]

    scope :asc, -> { order(position: :asc) }
    scope :desc, -> { order(position: :desc) }
    scope :active, -> { unscoped.where(active: true) }
    scope :passive, -> { unscoped.where(active: false) }

    after_create :create_option_values

    alias_attribute :multiply_by, :price_multiplier

    private
    # Creates option values when admin creates a language pair
    def create_option_values
      ::Spree::OptionType.find_or_create_for_order_category.option_values.create(name: "#{name.parameterize}", presentation: "#{name}", owner: self)
    end
  end
end
