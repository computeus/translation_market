module TranslationMarket
  class Base < Spree::Base
    self.abstract_class = true

    def self.table_name_prefix
      'translation_market_'
    end
  end
end
