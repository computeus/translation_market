# == Schema Information
#
# Table name: spree_language_pairs
#
#  id                 :integer          not null, primary key
#  source_language_id :integer
#  target_language_id :integer
#  active             :boolean          default(FALSE)
#  word_price         :decimal(10, 2)   default(0.0)
#  translator_price   :decimal(10, 2)   default(0.0)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

module TranslationMarket
  class LanguagePair < TranslationMarket::Base
    has_one :option_value, as: :owner, dependent: :destroy, inverse_of: :owner, class_name: 'Spree::OptionValue'

    belongs_to :source_language, inverse_of: :source_language_pairs, class_name: 'TranslationMarket::Language'
    belongs_to :target_language, inverse_of: :target_language_pairs, class_name: 'TranslationMarket::Language'
    belongs_to :store, inverse_of: :language_pairs, class_name: 'Spree::Store'

    validates :source_language, presence: true
    validates :source_language_id, uniqueness: { scope: [:target_language_id, :store_id] }
    validates :target_language, presence: true
    validates :target_language_id, uniqueness: { scope: [:source_language_id, :store_id] }
    validates :active, inclusion: { in: [true, false] }
    validates :store, presence: true
    validate :cant_pair_with_self

    after_create :create_option_values

    alias_attribute :multiply_by, :word_price

    self.whitelisted_ransackable_attributes = %w[source_language_id target_language_id word_price translator_price active activated]
    self.whitelisted_ransackable_associations = %w[source_language target_language]

    scope :active, -> { unscoped.where(active: true) }
    scope :passive, -> { unscoped.where(active: false) }

    def reverse_language_pair
      TranslationMarket::LanguagePair.find_by(source_language_id: target_language_id, target_language_id: source_language_id)
    end
    alias :reverse :reverse_language_pair

    private
    def cant_pair_with_self
      if source_language_id == target_language_id && source_language_id.present?
        errors.add(:target_language_id, "can't be the same as source language")
      end
    end

    # Creates option values when admin creates a language pair
    def create_option_values
      ::Spree::OptionType.find_or_create_for_language_pair.option_values.create(name: "#{source_language.code}-#{target_language.code}", presentation: "#{source_language.code} -> #{target_language.code}", owner: self)
    end
  end
end
