# This is the primary location for defining spree preferences
#
# The expectation is that this is created once and stored in
# the spree environment
#
# setters:
# a.color = :blue
# a[:color] = :blue
# a.set :color = :blue
# a.preferred_color = :blue
#
# getters:
# a.color
# a[:color]
# a.get :color
# a.preferred_color
#
require "spree/core/search/base"
require "spree/core/search/variant"

module TranslationMarket
  class AppConfiguration < Spree::Preferences::Configuration
    # Alphabetized to more easily lookup particular preferences

    # @!attribute [rw] admin_language_pairs_per_page
    #   @return [Integer] Number of language_pairs to display in admin (default: +20+)
    preference :admin_language_pairs_per_page, :integer, default: 20

    # @!attribute [rw] admin_order_languages_per_page
    #   @return [Integer] Number of languages to display in admin (default: +20+)
    preference :admin_languages_per_page, :integer, default: 20

    # @!attribute [rw] admin_order_categories_per_page
    #   @return [Integer] Number of order_categories to display in admin (default: +20+)
    preference :admin_order_categories_per_page, :integer, default: 20
  end
end
