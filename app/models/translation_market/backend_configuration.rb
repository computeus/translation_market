module TranslationMarket
  class BackendConfiguration < Spree::Preferences::Configuration
    preference :locale, :string, default: Rails.application.config.i18n.default_locale

    LANGUAGEPAIR_TABS  ||= [:language_pairs]
    ORDERCATEGORY_TABS ||= [:order_categories]
    LANGUAGE_TABS      ||= [:languages]
  end
end
