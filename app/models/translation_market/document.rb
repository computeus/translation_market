# == Schema Information
#
# Table name: spree_assets
#
#  id                      :integer          not null, primary key
#  viewable_id             :integer
#  viewable_type           :string(255)
#  attachment_width        :integer
#  attachment_height       :integer
#  attachment_file_size    :integer
#  position                :integer
#  attachment_content_type :string(255)
#  attachment_file_name    :string(255)
#  type                    :string(75)
#  attachment_updated_at   :datetime
#  alt                     :text(65535)
#  created_at              :datetime
#  updated_at              :datetime
#  word_count              :integer          default(0)
#

# Model derived from Spree::Asset to hold our uploaded files

module TranslationMarket
  class Document < Spree::Asset
    include TranslationMarket::Countable

    validate :no_attachment_errors

    has_attached_file :attachment,
                      url:            '/spree/products/:id/:basename.:extension',
                      path:           ':rails_root/public/spree/products/:id/:basename.:extension',
                      preserve_files: true
    validates_attachment :attachment,
                         :presence     => true,
                         :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document text/plain) },
                         :size         => { :in => 0..50.megabytes }

    # if there are errors from the plugin, then add a more meaningful message
    def no_attachment_errors
      unless attachment.errors.empty?
        # uncomment this to get rid of the less-than-useful interim messages
        # errors.clear
        errors.add :attachment, "Paperclip returned errors for file '#{attachment_file_name}' - check ImageMagick installation or image source file."
        false
      end
    end
  end
end
