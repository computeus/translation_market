# == Schema Information
#
# Table name: spree_languages
#
#  id         :integer          not null, primary key
#  code       :string(255)
#  position   :integer
#  active     :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module TranslationMarket
  class Language < TranslationMarket::Base
    acts_as_list

    has_many :source_language_pairs, inverse_of: :source_language, foreign_key: :source_language_id, class_name: 'TranslationMarket::LanguagePair'
    has_many :target_language_pairs, inverse_of: :target_language, foreign_key: :target_language_id, class_name: 'TranslationMarket::LanguagePair'
    has_many :target_languages, -> { uniq }, through: :source_language_pairs

    validates :code, length: { in: 1..7 }, presence: true, uniqueness: true
    validates :active, inclusion: { in: [true, false] }

    self.whitelisted_ransackable_attributes = %w[code active activated]

    scope :asc, -> { order(position: :asc) }
    scope :desc, -> { order(position: :desc) }
    scope :active, -> { unscoped.where(active: true) }
    scope :passive, -> { unscoped.where(active: false) }

    def self.source_languages
      where(id: TranslationMarket::LanguagePair.select(:source_language_id).uniq)
    end

    def self.target_languages
      where(id: TranslationMarket::LanguagePair.select(:target_language_id).uniq)
    end
  end
end
