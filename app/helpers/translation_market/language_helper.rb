module TranslationMarket
  module LanguageHelper
    def select_for_source_languages
      select_for_languages(:source_language, @source_languages, class: 'form-control')
    end

    def select_for_target_languages
      select_for_languages(:target_languages, @target_languages, class: 'form-control', multiple: true)
    end

    # Generic select tag output for languages
    def select_for_languages(name, elements, options)
      select_tag(name, options_for_select(elements.collect { |l| [l.code, l.id] }), options)
    end
  end
end
