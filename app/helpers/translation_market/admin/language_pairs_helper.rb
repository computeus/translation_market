module TranslationMarket
  module Admin
    module LanguagePairsHelper
      # Outputs language pair as string
      # with localization
      # examle: 'Turkish > English'
      def language_pair_as_string(language_pair)
        "#{I18n.t("languages.#{language_pair.source_language.code}")} > #{I18n.t("languages.#{language_pair.target_language.code}")}"
      end
    end
  end
end
