module TranslationMarket
  module OrderCategoryHelper
    # Generic select tag output for order categories
    def select_for_order_categories
      select_tag(:order_category, options_for_select(@order_categories.collect { |l| [l.code, l.id] }), class: 'form-control')
    end
  end
end