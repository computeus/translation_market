module TranslationMarket
  class StoresController < Spree::BaseController
    layout 'application'

    before_action :set_spree_store, only: [:show, :edit, :update, :destroy]

    # GET /spree/stores/1
    # GET /spree/stores/1.json
    def show
    end

    # GET /spree/stores/new
    def new
      @spree_user = Spree::User.new
      @spree_user.store = @spree_user.build_store
    end

    # GET /spree/stores/1/edit
    def edit
    end

    # POST /spree/stores
    # POST /spree/stores.json
    def create
      @spree_user = Spree::User.build_store_admin(spree_user_params)

      respond_to do |format|
        if @spree_user.save
          sign_in(@spree_user, :event => :authentication, :bypass => true)
          format.html { redirect_to @spree_user.store, notice: 'Store was successfully created.' }
          format.json { render :show, status: :created, location: @spree_store }
        else
          format.html { render :new }
          format.json { render json: @spree_user.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /spree/stores/1
    # PATCH/PUT /spree/stores/1.json
    def update
      respond_to do |format|
        if @spree_store.update(spree_store_params)
          format.html { redirect_to @spree_store, notice: 'Store was successfully updated.' }
          format.json { render :show, status: :ok, location: @spree_store }
        else
          format.html { render :edit }
          format.json { render json: @spree_store.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /spree/stores/1
    # DELETE /spree/stores/1.json
    def destroy
      @spree_store.destroy
      respond_to do |format|
        format.html { redirect_to stores_url, notice: 'Store was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_spree_store
      @spree_store = spree_current_user.store
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def spree_store_params
      params.require(:spree_store).permit(:name, :url, :mail_from_address)
    end

    def spree_user_params
      params.require(:user).permit(:email, :password, store_attributes: [:name, :url, :mail_from_address])
    end
  end
end