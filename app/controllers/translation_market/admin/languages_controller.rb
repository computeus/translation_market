module TranslationMarket
  module Admin
    class LanguagesController < TranslationMarket::Admin::ResourceController
      before_action :set_language, only: [:edit, :update]

      # GET /translation_market/admin/languages
      # GET /translation_market/admin/languages.json
      def index
        @languages_for_search = TranslationMarket::Language.all.collect { |language| [I18n.t("languages.#{language.code}"), language.id] }
        respond_with(@collection) do |format|
          format.html
        end
      end

      # GET /translation_market/admin/languages/1/edit
      def edit
      end

      # PATCH/PUT /translation_market/admin/languages/1
      # PATCH/PUT /translation_market/admin/languages/1.json
      def update
        respond_to do |format|
          if @language.update(language_params)
            flash[:success] = TranslationMarket.t(:updated_successfully)
            format.html { redirect_to main_app.admin_language_path(@language), success: TranslationMarket.t(:updated_successfully) }
          else
            format.html { render :edit }
          end
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_language
        @language = TranslationMarket::Language.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def language_params
        params.require(:language).permit(:active)
      end

      # Overrides collection method to set proper variable and data
      def collection
        return @collection if @collection.present?

        # Display all records by default.
        params[:q]             ||= {}
        params[:q][:activated] ||= '0'

        @collection = TranslationMarket::Language.all
        @collection = @collection.active if params[:q][:activated] == '1'
        @search     = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(TranslationMarket::Config[:admin_languages_per_page])
      end
    end
  end
end
