module TranslationMarket
  module Admin
    class ResourceController < Spree::Admin::ResourceController
      # Overrides method for proper class name
      def model_class
        "TranslationMarket::#{controller_name.classify}".constantize
      end
    end
  end
end