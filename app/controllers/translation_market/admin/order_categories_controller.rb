module TranslationMarket
  module Admin
    class OrderCategoriesController < TranslationMarket::Admin::ResourceController
      before_action :set_order_category, only: [:edit, :update]

      # GET /translation_market/admin/order_categories
      # GET /translation_market/admin/order_categories.json
      def index
        @order_categories_for_search = TranslationMarket::OrderCategory.all.collect{ |order_category| [order_category.name, order_category.id] }
        respond_with(@collection) do |format|
          format.html
        end
      end

      # GET /translation_market/admin/order_categories/1/edit
      def edit
      end

      # PATCH/PUT /translation_market/admin/order_categories/1
      # PATCH/PUT /translation_market/admin/order_categories/1.json
      def update
        respond_to do |format|
          if @order_category.update(order_category_params)
            flash[:success] = TranslationMarket.t(:updated_successfully)
            format.html { redirect_to main_app.edit_admin_order_category_path(@order_category), success: TranslationMarket.t(:updated_successfully) }
          else
            format.html { render :edit }
          end
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_order_category
        @order_category = TranslationMarket::OrderCategory.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def order_category_params
        params.require(:translation_market_order_category).permit(:description, :active, :price_multiplier, :delivery_time_multiplier)
      end

      # Overrides collection method to set proper variable and data
      def collection
        return @collection if @collection.present?

        params[:q]             ||= {}
        params[:q][:activated] ||= '0'

        @collection = TranslationMarket::OrderCategory.all
        @collection = @collection.active if params[:q][:activated] == '1'
        @search     = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(TranslationMarket::Config[:admin_order_categories_per_page])
      end
    end
  end
end