module TranslationMarket
  module Admin
    class LanguagePairsController < TranslationMarket::Admin::ResourceController
      before_action :set_language_pair, only: [:edit, :update, :destroy]
      before_action :set_languages

      # GET /translation_market/admin/language_pairs
      # GET /translation_market/admin/language_pairs.json
      def index
        respond_with(@collection) do |format|
          format.html
        end
      end

      # GET /translation_market/admin/language_pairs/new
      def new
        @language_pair = TranslationMarket::LanguagePair.new
      end

      # GET /translation_market/admin/language_pairs/1/edit
      def edit
      end

      # POST /translation_market/admin/language_pairs
      # POST /translation_market/admin/language_pairs.json
      def create
        @language_pair = TranslationMarket::LanguagePair.new(language_pair_params)

        respond_to do |format|
          if @language_pair.save

            flash[:success] = TranslationMarket.t(:created_successfully)
            format.html { redirect_to main_app.admin_language_pairs_path, success: TranslationMarket.t(:created_successfully) }
          else
            format.html { render :new }
          end
        end
      end

      # PATCH/PUT /translation_market/admin/language_pairs/1
      # PATCH/PUT /translation_market/admin/language_pairs/1.json
      def update
        respond_to do |format|
          if @language_pair.update(language_pair_params)

            flash[:success] = TranslationMarket.t(:updated_successfully)
            format.html { redirect_to main_app.admin_language_pairs_path, success: TranslationMarket.t(:updated_successfully) }
          else
            format.html { render :edit }
          end
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_language_pair
        @language_pair = TranslationMarket::LanguagePair.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def language_pair_params
        params.require(:translation_market_language_pair).permit(:source_language_id, :target_language_id, :active, :word_price, :translator_price)
      end

      # Overrides collection method to set proper variable and data
      def collection
        return @collection if @collection.present?

        # Display all records by default.
        params[:q]             ||= {}
        params[:q][:activated] ||= '0'

        @collection = TranslationMarket::LanguagePair.includes(:source_language, :target_language)
        @collection = @collection.active if params[:q][:activated] == '1'
        @search     = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(TranslationMarket::Config[:admin_language_pairs_per_page])
      end

      # Sets active languages as @languages for language pair create action.
      def set_languages
        @languages = TranslationMarket::Language.active.asc.collect { |language| [I18n.t("languages.#{language.code}"), language.id] }
      end
    end
  end
end