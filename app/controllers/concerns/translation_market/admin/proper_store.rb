# Module for proper store redirection
module TranslationMarket
  module Admin
    module ProperStore
      extend ActiveSupport::Concern

      private
      # Checks if current user's store is currently visited store.
      def proper_store?
        current_store == spree_current_user.store
      end

      # Returns current store admin's store url
      def proper_store_url
        if spree_current_user.present? && !proper_store?
          user = spree_current_user
          sign_out(spree_current_user)
          flash.discard(:success)
          spree.admin_url(host: user.store.url, message: TranslationMarket.t(:wrong_domain))
        end
      end
    end
  end
end