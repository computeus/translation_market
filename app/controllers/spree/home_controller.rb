module Spree
  class HomeController < Spree::StoreController
    helper 'spree/products'
    layout 'application'
    respond_to :html

    # Overrides action with our custom variables set
    def index
      @searcher         = build_searcher(params.merge(include_images: true))
      @products         = @searcher.retrieve_products
      @taxonomies       = Spree::Taxonomy.includes(root: :children)
      @order_categories = TranslationMarket::OrderCategory.all
      @source_languages = TranslationMarket::Language.source_languages
      @target_languages = TranslationMarket::Language.target_languages
    end
  end
end
