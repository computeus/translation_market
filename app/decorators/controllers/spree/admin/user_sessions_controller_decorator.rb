module Spree
  module Admin
    UserSessionsController.class_eval do
      include TranslationMarket::Admin::ProperStore

      # Display error message caused by proper store redirection
      def new
        flash.now[:error] = params[:message] if params[:message].present?
      end

      private
      # Overrides redirection with proper_store_url
      def redirect_back_or_default(default)
        redirect_to(proper_store_url || session["spree_user_return_to"] || default)
        session["spree_user_return_to"] = nil
      end
    end
  end
end
