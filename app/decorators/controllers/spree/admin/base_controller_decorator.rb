module Spree
  module Admin
    BaseController.class_eval do
      # Stores the store admin's last requested url
      # if store admin is not logged in
      self.unauthorized_redirect = Proc.new {
        if try_spree_current_user
          flash[:error] = Spree.t(:authorization_failure)
          redirect_to spree.admin_unauthorized_path
        else
          store_location
          redirect_to spree.admin_login_path(message: params[:message])
        end
      }
    end
  end
end
