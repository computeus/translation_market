module Spree
  module Admin
    ProductsController.class_eval do
      private
      # Search in current stores products.
      def find_resource
        current_store.products.with_deleted.friendly.find(params[:id])
      end

      # Search in current stores products
      def collection
        return @collection if @collection.present?
        params[:q] ||= {}
        params[:q][:deleted_at_null] ||= "1"

        params[:q][:s] ||= "name asc"
        @collection = current_store.products
        @collection = @collection.with_deleted if params[:q].delete(:deleted_at_null) == '0'
        # @search needs to be defined as this is passed to search_form_for
        @search = @collection.ransack(params[:q])
        @collection = @search.result.
              distinct_by_product_ids(params[:q][:s]).
              includes(product_includes).
              page(params[:page]).
              per(Spree::Config[:admin_products_per_page])

        @collection
      end

      # Added documents relation for includes directive
      def product_includes
        [{ variants: [:images, option_values: :option_type], master: [:images, :default_price] }, :documents]
      end
    end
  end
end
