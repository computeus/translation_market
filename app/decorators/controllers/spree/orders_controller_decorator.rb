module Spree
  OrdersController.class_eval do
    before_action :create_product, only: :populate

    # Overrides order population.
    def populate
      order    = current_order(create_order_if_necessary: true)
      quantity = 1

      # 2,147,483,647 is crazy. See issue https://github.com/spree/spree/issues/2695.
      if quantity.between?(1, 2_147_483_647)
        begin
          # Every variant created for any uploaded document
          # must be added once to order
          @product.variants.each do |variant|
            order.contents.add(variant, quantity)
          end
        rescue ActiveRecord::RecordInvalid => e
          error = e.record.errors.full_messages.join(", ")
        end
      else
        error = Spree.t(:please_enter_reasonable_quantity)
      end

      if error
        flash[:error] = error
        redirect_back_or_default(spree.root_path)
      else
        respond_with(order) do |format|
          format.html { redirect_to cart_path }
        end
      end
    end

    # Creates a product for every uploaded document with option values
    def create_product
      @product = Spree::Product.create(store: current_store, option_values_hash: option_values_hash, documents_attributes: [{ attachment: product_params[:file] }])
    end

    # Creates a hash formed from language pair and order category option values
    def option_values_hash
      language_pair_option_values = Spree::OptionValue.from_language_pairs(TranslationMarket::LanguagePair.where(source_language_id: product_params[:source_language], target_language_id: product_params[:target_languages]))

      order_category_option_values = [TranslationMarket::OrderCategory.find(product_params[:order_category]).option_value.id]

      { Spree::OptionType.find_or_create_for_language_pair.id.to_s => language_pair_option_values, Spree::OptionType.find_or_create_for_order_category.id.to_s => order_category_option_values }
    end

    private
    # Render 404 if any product params is absent
    def product_params
      render_404 if params[:source_language].blank?
      render_404 if params[:target_languages].blank?
      render_404 if params[:order_category].blank?
      render_404 if params[:file].blank?

      { source_language: params[:source_language], target_languages: params[:target_languages], order_category: params[:order_category], file: params[:file] }
    end
  end
end
