module Spree
  Variant.class_eval do
    has_many :documents, -> { order(:position) }, as: :viewable, dependent: :destroy, class_name: "TranslationMarket::Document"

    before_create :set_variant_price

    private
    def set_variant_price
      option_values.each do |ov|
        self.price = price * ov.owner.multiply_by if ov.owner.present?
      end
    end
  end
end