module Spree
  Role.class_eval do
    def store_admin?
      name == 'store_admin'
    end
  end
end
