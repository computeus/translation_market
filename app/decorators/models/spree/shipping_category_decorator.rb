module Spree
  ShippingCategory.class_eval do
    def self.default
      find_by(name: 'Default')
    end
  end
end