module Spree
  Order.class_eval do
    scope :by_store, lambda { |store| where(store: store) }
  end
end
