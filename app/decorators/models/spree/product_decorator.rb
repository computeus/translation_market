module Spree
  Product.class_eval do
    delegate :documents, to: :master, prefix: true
    alias_method :translated_documents, :master_documents

    belongs_to :store, inverse_of: :products

    has_many :variant_documents, -> { order(:position) }, source: :documents, through: :variants_including_master
    has_many :documents, -> { order(:position) }, as: :viewable, dependent: :destroy, class_name: "TranslationMarket::Document"

    accepts_nested_attributes_for :documents, allow_destroy: true, reject_if: lambda { |d| d[:attachment].blank? }

    after_initialize :ensure_shipping_category
    after_initialize :set_product_name, if: :new_record?
    after_create :set_price

    validates :store, presence: true

    def ensure_shipping_category
      return unless new_record?
      return if shipping_category.present?
      self.shipping_category = Spree::ShippingCategory.default
    end

    # Creates variants from documents and language pairs nad order categories
    def build_variants_from_option_values_hash
      count_words_of_uploaded_documents
      ensure_option_types_exist_for_values_hash
      values = option_values_hash.values
      values = values.inject(values.shift) { |memo, value| memo.product(value).map(&:flatten) }

      documents.each do |d|
        values.each do |ids|
          variants.create!(option_value_ids: ids, price: d.word_count)
        end
      end
      save
    end

    def count_words_of_uploaded_documents
      documents.each { |d| d.count_words }
    end

    # Set product name from time
    def set_product_name
      loop do
        token = Time.now.strftime('%Y%m%d%H%M%S%L')
        if !Spree::Product.find_by(name: token)
          self.name = token
          break
        end
      end if name.blank?
    end

    # Set price from variants.
    # Sum of variants price
    def set_price
      self.price = variants.collect(&:price).sum
      save!
    end
  end
end
