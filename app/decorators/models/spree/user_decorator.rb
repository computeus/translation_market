module Spree
  User.class_eval do
    has_one :store, inverse_of: :admin, dependent: :destroy

    scope :store_admin, -> { includes(:spree_roles).where("#{Spree::Role.table_name}.name" => 'store_admin') }
    scope :customer, -> { includes(:spree_roles).where("#{Spree::Role.table_name}.name" => 'user') }

    accepts_nested_attributes_for :store, allow_destroy: false

    def store_admin?
      can?(:manage, :store)
    end

    def self.build_store_admin(attributes)
      user = self.new(attributes)
      user.spree_roles << Spree::Role.find_by(name: 'store_admin')
      user
    end

    def ability
      @ability ||= Spree::Ability.new(self)
    end
    delegate :can?, :cannot?, to: :ability
  end
end