module Spree
  OptionType.class_eval do
    def self.find_or_create_for_language_pair
      OptionType.find_or_create_by(name: 'language-pair', presentation: 'Language Pair')
    end

    def self.find_or_create_for_order_category
      OptionType.find_or_create_by(name: 'order-category', presentation: 'Order Category')
    end
  end
end
