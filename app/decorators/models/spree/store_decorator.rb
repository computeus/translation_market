module Spree
  Store.class_eval do
    has_many :products, inverse_of: :store, dependent: :destroy
    has_many :orders, inverse_of: :store, dependent: :destroy
    has_many :language_pairs, inverse_of: :store, dependent: :destroy, class_name: 'TranslationMarket::LanguagePair'
    
    belongs_to :admin, class_name: 'Spree::User', foreign_key: :user_id, inverse_of: :store

    validates :admin, presence: true
    validate :is_admin_store_admin?

    before_validation :set_code

    private
    def set_code
      self.code = self.name.parameterize
    end

    def ensure_default_exists_and_is_unique
      if default
        Store.where.not(id: id).update_all(default: false)
      elsif Store.where(default: true).count == 0
        self.default = true
      end
    end

    # Only users with store admin role can be store owner
    def is_admin_store_admin?
      if !admin.store_admin?
        errors[:admin] << 'is not a store admin!'
      end
    end
  end
end
