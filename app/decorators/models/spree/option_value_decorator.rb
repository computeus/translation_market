module Spree
  OptionValue.class_eval do
    belongs_to :owner, inverse_of: :option_value, polymorphic: true

    clear_validators!
    validates :presentation, presence: true

    def self.from_language_pairs(language_pairs)
      language_pairs.includes(:option_value).collect(&:option_value).collect(&:id)
    end
  end
end
