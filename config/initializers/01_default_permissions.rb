require 'spree/permission_sets'

Spree::RoleConfiguration.configure do |config|
  config.assign_permissions :store_admin, [Spree::PermissionSets::StoreAdmin]
end