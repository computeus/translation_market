Dir.glob(File.join(File.dirname(__FILE__), "../../lib/**/*.rb")) do |c|
  Rails.configuration.cache_classes ? require(c) : load(c)
end